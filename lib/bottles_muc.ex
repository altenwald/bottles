defmodule Bottles.Muc do

    @moduledoc """
    Provides convenience functions for building XMPP stanzas for MUC
    """

    @doc """
    Build a stanza for get_room_opts.
    
    #Example:

        Stanza:
        <iq type='get' to='xmppdomain' id='get_room_opts'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='opts'/>
                <rid>roomid</rid>
            </query>
        </iq>

        iex(1)> stanza = Bottles.Muc.get_room_opts('server.com', '123456')   
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "3a1f"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "opts"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)                              
        "<iq to='server.com' type='get' id='3a1f'><query xmlns='urn:xmpp:muc:light:1'><action opcode='opts'/><rid>123456</rid></query></iq>"
    """
    def get_room_opts(to, roomid) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("opts"), rid]}

        Hedwig.Stanza.iq(to, 'get', query)  
    end

    @doc """
    Build a stanza for get_room_members.

    #Example:

        Stanza:
        <iq type='get' to='xmppdomain' id='get_room_members'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='room_members'/>
                <user>username</user> -> optional this is to range search/paging on members list as this can be huge
            </query>
        </iq> 

        iex(8)> stanza = Bottles.Muc.get_room_members('server.com', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "ee92"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "room_members"}], []},
             {:xmlel, "user", [], [xmlcdata: "mynick"]}]}}
        iex(9)> Hedwig.Stanza.to_xml(stanza)                                 
        "<iq to='server.com' type='get' id='ee92'><query xmlns='urn:xmpp:muc:light:1'><action opcode='room_members'/><user>mynick</user></query></iq>"
    """
    def get_room_members(to, username) do
        case username do
            :undefined ->
                query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("room_members")]}    
            _          ->       
                user = build_item("user", [], [:exml.escape_cdata(username)])

                query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("room_members"), user]}
        end

        Hedwig.Stanza.iq(to, 'get', query)
    end

    @doc """
    Build a stanza for get_my_rooms.

    #Example:

        Stanza:
        <iq type='get' to='xmppdomain' id='get_my_rooms'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='rooms'/>
            </query>
        </iq>

        iex(6)> stanza = Bottles.Muc.get_my_rooms('server.com')             
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "cf8c"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "rooms"}], []}]}}
        iex(7)> Hedwig.Stanza.to_xml(stanza)                   
        "<iq to='server.com' type='get' id='cf8c'><query xmlns='urn:xmpp:muc:light:1'><action opcode='rooms'/></query></iq>"
    """    
    def get_my_rooms(to) do
        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("rooms")]}

        Hedwig.Stanza.iq(to, 'get', query)
    end

    @doc """
    Build a stanza for get_mute_period.

    #Example:

        Stanza:
        <iq type='get' to='xmppdomain' id='get_mute_period'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='mute'/>
                <rid>roomid</rid>
            </query>
        </iq>

        iex(4)> stanza = Bottles.Muc.get_mute_period('server.com', '123456')           
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "6919"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "mute"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]}]}}
        iex(5)> Hedwig.Stanza.to_xml(stanza)                                
        "<iq to='server.com' type='get' id='6919'><query xmlns='urn:xmpp:muc:light:1'><action opcode='mute'/><rid>123456</rid></query></iq>"
    """
    def get_mute_period(to, roomid) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("mute"), rid]}

        Hedwig.Stanza.iq(to, 'get', query)
    end

    @doc """
    Build a stanza for searching room by name.
    
    #Example:

        Stanza:
        <iq type='get' to='xmppdomain' id='search_room_by_name'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='search'/>
                <subject>roomname</subject>
            </query>
        </iq> 

        iex(13)> stanza = Bottles.Muc.search_room_by_name('server.com', 'myroom')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "7d7f"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "search"}], []},
             {:xmlel, "subject", [], [xmlcdata: "myroom"]}]}}
        iex(14)> Hedwig.Stanza.to_xml(stanza)                                    
        "<iq to='server.com' type='get' id='7d7f'><query xmlns='urn:xmpp:muc:light:1'><action opcode='search'/><subject>myroom</subject></query></iq>"
    """
    def search_room_by_name(to, roomname) do
        subject = build_item("subject", [], [:exml.escape_cdata(roomname)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("search"), subject]}

        Hedwig.Stanza.iq(to, 'get', query)
    end 

    @doc """
    Build a stanza for getting blocked users for room.
    
    #Example:

        Stanza:
        <iq type='get' to='xmppdomain' id='get_blocked_users_for_room'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='blocked'/>
            </query>
        </iq>
        
        iex(2)> stanza = Bottles.Muc.get_blocked_users_for_room('server.com')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "c3a8"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "blocked"}], []}]}}
        iex(3)> Hedwig.Stanza.to_xml(stanza)
        "<iq to='server.com' type='get' id='c3a8'><query xmlns='urn:xmpp:muc:light:1'><action opcode='blocked'/></query></iq>"
    """
    def get_blocked_users_for_room(to) do
        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("blocked")]}

        Hedwig.Stanza.iq(to, 'get', query)
    end

    @doc """
    Build a stanza for blocking user.
    
    #Example:

        Stanza:
        <iq type='set' to='xmppdomain' id='block_user'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='block'/>
                <rid >room id</rid>
                <user>username</user>
            </query>
        </iq>

        iex(1)> stanza = Bottles.Muc.block_user('server.com', '123456', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "758a"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "block"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "user", [], [xmlcdata: "mynick"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)
        "<iq to='server.com' type='set' id='758a'><query xmlns='urn:xmpp:muc:light:1'><action opcode='block'/><rid>123456</rid><user>mynick</user></query></iq>"        
    """
    def block_user(to, roomid, username) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        user = build_item("user", [], [:exml.escape_cdata(username)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("block"), rid, user]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for unblocking user.

    #Example:

        Stanza:
        <iq type='set' to='xmppdomain' id='block_user'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='block'/>
                <rid >room id</rid>
                <user>username</user>
            </query>
        </iq>

        iex(1)> stanza = Bottles.Muc.unblock_user('server.com', '123456', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "407b"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "block"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "user", [], [xmlcdata: "mynick"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)
        "<iq to='server.com' type='get' id='407b'><query xmlns='urn:xmpp:muc:light:1'><action opcode='block'/><rid>123456</rid><user>mynick</user></query></iq>"
    """
    def unblock_user(to, roomid, username) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        user = build_item("user", [], [:exml.escape_cdata(username)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("block"), rid, user]}

        Hedwig.Stanza.iq(to, 'get', query)
    end

    @doc """
    Build a stanza for creating room.

    #Example:

        Stanza:
        <iq type='set' to='xmppdomain' id='create'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='create'/>
                <avatar>avatar url</avatar>
                <subject>roomname</subject>
                <room_type>public | private</room_type>
                <nick>avatar user nick | admin nick</nick>
            </query>
        </iq> 

        iex(5)> stanza = Bottles.Muc.create_room('server.com', 'http://myavatar.com/1', 'myroom', 'public', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "cff8"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "create"}], []},
             {:xmlel, "avatar", [], [xmlcdata: "http://myavatar.com/1"]},
             {:xmlel, "subject", [], [xmlcdata: "myroom"]},
             {:xmlel, "room_type", [], [xmlcdata: "public"]},
             {:xmlel, "nick", [], [xmlcdata: "mynick"]}]}}
        iex(6)> Hedwig.Stanza.to_xml(stanza)                                                                         
        "<iq to='server.com' type='set' id='cff8'><query xmlns='urn:xmpp:muc:light:1'><action opcode='create'/><avatar>http://myavatar.com/1</avatar><subject>myroom</subject><room_type>public</room_type><nick>mynick</nick></query></iq>"
    """
    def create_room(to, avatar_url, roomname, room_type, nickname) do
        avatar = build_item("avatar", [], [:exml.escape_cdata(avatar_url)])
        
        subject = build_item("subject", [], [:exml.escape_cdata(roomname)])

        roomtype = build_item("room_type", [], [:exml.escape_cdata(room_type)])

        nick = build_item("nick", [], [:exml.escape_cdata(nickname)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("create"), avatar, subject, roomtype, nick]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for destroying room.

    #Example:
    
        Stanza:
        <iq type='set' to='xmppdomain' id='destroy'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='destroy'/>
                <rid>roomid</rid>
            </query>
        </iq> 

        iex(1)> stanza = Bottles.Muc.destroy_room('server.com', '123456')                                            
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "a1e9"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "destroy"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)                             
        "<iq to='server.com' type='set' id='a1e9'><query xmlns='urn:xmpp:muc:light:1'><action opcode='destroy'/><rid>123456</rid></query></iq>"
    """
    def destroy_room(to, roomid) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("destroy"), rid]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for setting room opts.

    #Example:
    
        Stanza:
        <iq type='set' to='xmppdomain' id='opts'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='opts'/>
                <rid>roomid</rid>
                <avatar>avatar url</avatar>
                <subject>roomname</subject>
                <room_type>public | private</room_type>
                <nick>avatar user nick | admin nick</nick>
            </query>
        </iq> 

        iex(5)> stanza = Bottles.Muc.set_room_opts('server.com', '123456', 'http://avatar/1', 'myroom', 'private', 'mynick') 
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "22a3"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "opts"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "avatar", [], [xmlcdata: "http://avatar/1"]},
             {:xmlel, "subject", [], [xmlcdata: "myroom"]},
             {:xmlel, "room_type", [], [xmlcdata: "private"]},
             {:xmlel, "nick", [], [xmlcdata: "mynick"]}]}}
        iex(6)> Hedwig.Stanza.to_xml(stanza)                                                                                 
        "<iq to='server.com' type='set' id='22a3'><query xmlns='urn:xmpp:muc:light:1'><action opcode='opts'/><rid>123456</rid><avatar>http://avatar/1</avatar><subject>myroom</subject><room_type>private</room_type><nick>mynick</nick></query></iq>"
    """
    def set_room_opts(to, roomid, avatar_url, roomname, room_type, nickname) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        avatar = build_item("avatar", [], [:exml.escape_cdata(avatar_url)])
        
        subject = build_item("subject", [], [:exml.escape_cdata(roomname)])

        roomtype = build_item("room_type", [], [:exml.escape_cdata(room_type)])

        nick = build_item("nick", [], [:exml.escape_cdata(nickname)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("opts"), rid, avatar, subject, roomtype, nick]}

        Hedwig.Stanza.iq(to, 'set', query)
    end


    @doc """
    Build a stanza for adding admin.

    #Example:

        Stanza:
        <iq type='set' to='xmppdomain' id='add_admin'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='add_admin'/>
                <rid>roomid</rid>
                <user>username</user>
            </query>
        </iq>

        iex(1)> stanza = Bottles.Muc.add_admin('server.com', '123456', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "1d41"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "add_admin"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "user", [], [xmlcdata: "mynick"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)
        "<iq to='server.com' type='set' id='1d41'><query xmlns='urn:xmpp:muc:light:1'><action opcode='add_admin'/><rid>123456</rid><user>mynick</user></query></iq>" 
    """
    def add_admin(to, roomid, username) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        user = build_item("user", [], [:exml.escape_cdata(username)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("add_admin"), rid, user]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for removing admin.

    #Example:

        Stanza:
        <iq type='set' to='xmppdomain' id='remove_admin'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='remove_admin'/>
                <rid>roomid</rid>
                <user>username</user>
            </query>
        </iq> 

        iex(1)> stanza = Bottles.Muc.remove_admin('server.com', '123456', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "d69b"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "remove_admin"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "user", [], [xmlcdata: "mynick"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)                                       
        "<iq to='server.com' type='set' id='d69b'><query xmlns='urn:xmpp:muc:light:1'><action opcode='remove_admin'/><rid>123456</rid><user>mynick</user></query></iq>"
    """
    def remove_admin(to, roomid, username) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        user = build_item("user", [], [:exml.escape_cdata(username)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("remove_admin"), rid, user]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for adding user.
    
    #Example: 
    
        Stanza:
        <iq type='set' to='xmppdomain' id='add_user'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='add_user'/>
                <rid>roomid</rid>
                <user>username</user>
                <nick>username</nick>
                <subject>username</subject>
            </query>
        </iq>

        iex(3)> stanza = Bottles.Muc.add_user('server.com', '123456', 'myusername', 'mynick', 'mysubject')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "662f"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "add_user"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "user", [], [xmlcdata: "myusername"]},
             {:xmlel, "nick", [], [xmlcdata: "mynick"]},
             {:xmlel, "subject", [], [xmlcdata: "mysubject"]}]}}
        iex(4)> Hedwig.Stanza.to_xml(stanza)                                                              
        "<iq to='server.com' type='set' id='662f'><query xmlns='urn:xmpp:muc:light:1'><action opcode='add_user'/><rid>123456</rid><user>myusername</user><nick>mynick</nick><subject>mysubject</subject></query></iq>"
    """
    def add_user(to, roomid, username, nickname, subjectname) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        user = build_item("user", [], [:exml.escape_cdata(username)])

        nick = build_item("nick", [], [:exml.escape_cdata(nickname)])

        subject = build_item("subject", [], [:exml.escape_cdata(subjectname)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("add_user"), rid, user, nick, subject]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for leaving room.
    
    #Example:
    
        Stanza:
        <iq type='set' to='xmppdomain' id='leave_room'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='leave_room'/>
                <rid>roomid</rid>
            </query>
        </iq> 

        iex(9)> stanza = Bottles.Muc.leave_room('server.com', '123456')  
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "bd99"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "leave_room"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]}]}}
        iex(10)> Hedwig.Stanza.to_xml(stanza)                           
        "<iq to='server.com' type='set' id='bd99'><query xmlns='urn:xmpp:muc:light:1'><action opcode='leave_room'/><rid>123456</rid></query></iq>"
    """
    def leave_room(to, roomid) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("leave_room"), rid]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for removing user from room.
    
    #Example:
    
        Stanza:
        <iq type='set' to='xmppdomain' id='remove_user'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='remove_user'/>
                <rid>roomid</rid>
                <user>username</user>
            </query>
        </iq>

        iex(1)> stanza = Bottles.Muc.remove_user_from_room('server.com', '123456', 'mynick')
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "deb2"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "remove_user"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "user", [], [xmlcdata: "mynick"]}]}}
        iex(2)> Hedwig.Stanza.to_xml(stanza)                                                
        "<iq to='server.com' type='set' id='deb2'><query xmlns='urn:xmpp:muc:light:1'><action opcode='remove_user'/><rid>123456</rid><user>mynick</user></query></iq>"
    """
    def remove_user_from_room(to, roomid, username) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        user = build_item("user", [], [:exml.escape_cdata(username)])

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("remove_user"), rid, user]}

        Hedwig.Stanza.iq(to, 'set', query)
    end

    @doc """
    Build a stanza for setting mute period.
    
    #Example: 
    
        Stanza:
        <iq type='set' to='xmppdomain' id='mute'>
            <query xmlns='urn:xmpp:muc:light:1'>
                <action opcode='mute'/>
                <rid>roomid</rid>
                <mute>INF | INTEGER VALUE</mute>
            </query>
        </iq>

        iex(3)> stanza = Bottles.Muc.set_mute_period('server.com', '123456', 10)   
        {:xmlel, "iq", [{"to", 'server.com'}, {"type", 'set'}, {"id", "a014"}],
          {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
            [{:xmlel, "action", [{"opcode", "mute"}], []},
             {:xmlel, "rid", [], [xmlcdata: "123456"]},
             {:xmlel, "mute", [], [xmlcdata: "10"]}]}}
        iex(4)> Hedwig.Stanza.to_xml(stanza)                                    
        "<iq to='server.com' type='set' id='a014'><query xmlns='urn:xmpp:muc:light:1'><action opcode='mute'/><rid>123456</rid><mute>10</mute></query></iq>" 
    """
    def set_mute_period(to, roomid, mute) do
        rid = build_item("rid", [], [:exml.escape_cdata(roomid)])

        case mute do
            "INF" ->
                mute = build_item("mute", [], [:exml.escape_cdata("INF")])
             mute ->
                mute = build_item("mute", [], [:exml.escape_cdata(:erlang.integer_to_binary(mute))])
        end

        query = {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}], [build_action("mute"), rid, mute]}
       
        Hedwig.Stanza.iq(to, 'set', query)
    end

    #####################
    # Private functions #
    #####################

    # Build an action item 
    #
    #   <action opcode='code'>
    #
    defp build_action(action) do
        {:xmlel, "action", [{"opcode", action}], []}
    end

    #
    # Build item with: name, attrs and body
    #
    defp build_item(name, attrs, body) do
        {:xmlel, name, attrs, body} 
    end
end
