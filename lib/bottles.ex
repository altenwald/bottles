defmodule Bottles do
    @moduledoc """
    Provides convenience functions to connect, disconnect and send XMPP stanzas 
    """

    @doc """
    Create a new client map.

    #Example:

        iex(1)> client = Bottles.make_client('allison@zgbjggs-macbook-pro.local', '123456')
        %{handlers: [{Hedwig.Handlers.Receiver, %{}}],
        jid: "allison@zgbjggs-macbook-pro.local", nickname: "allison", password: "123456",
        resource: "bot", rooms: []}
    """
    def make_client(jid, password) do
        [nickname | _] = :string.tokens(jid, '@')
        %{ jid: :erlang.list_to_binary(jid),
           password: :erlang.list_to_binary(password),
           nickname: :erlang.list_to_binary(nickname),
           resource: "bot",
           rooms: [],
           handlers: [{Hedwig.Handlers.Receiver, %{}}]
        }
    end

    @doc """
    Connect the client against XMPP server.

    #Example

        iex(2)> Bottles.connect_client(client)
        {:ok, #PID<0.144.0>}
    """
    def connect_client(client) do

        # Start the client, the client is made with `make_client/2` 
        {:ok, _pid} = Hedwig.start_client(client)
    end


    @doc """
    Disconnect the client from XMPP server.

    #Example

        iex(6)> Bottles.disconnect_client(client)
        :ok
    """
    def disconnect_client(client) do
    
        # Get the pid of the client by the JID
        pid = Hedwig.whereis(client[:jid])

        # Stop the client.
        Hedwig.stop_client(pid)
    end

    @doc """
    Send stanza to the XMPP server.

    #Example
    
        iex(5)> BOT.send_stanza(client, stanza)
    """
    def send_stanza(client, stanza) do

        # Get the pid of the client by the JID
        pid = Hedwig.whereis(client[:jid])

        Hedwig.Client.reply(pid, stanza)
    end

end
