Bottles
==========

Bottles is a xmpp bot to send and receive stanzas to/from XMPP server using Elixir.

* NOTE: This BOT is only for testing purposes.

### **Dependencies:**
***

* Hedwig - XMPP Client Framework

### **How to use:**
***

* Download the code from bitbucket repo:

```bash
git clone git@bitbucket.org:altenwald/bottles.git
```

* Download dependencies and compile:

```bash
make 
```

* Start the Elixir Interactive Shell and play:

```bash
make start
```

* Create a client to connect against the server:

```elixir
iex(1)> client = Bottles.make_client('allison@zgbjggs-macbook-pro.local', '123456')
%{handlers: [{Hedwig.Handlers.Receiver, %{}}],
  jid: "allison@zgbjggs-macbook-pro.local", nickname: "allison", password: "123456",
  resource: "bot", rooms: []}
```

* Connect the client:

```elixir
iex(2)> Bottles.connect_client(client)
{:ok, #PID<0.144.0>}
```

* Create a stanza using muc:

```elixir
iex(6)> stanza = Bottles.Muc.get_my_rooms('server.com')             
{:xmlel, "iq", [{"to", 'server.com'}, {"type", 'get'}, {"id", "cf8c"}],
  {:xmlel, "query", [{"xmlns", "urn:xmpp:muc:light:1"}],
    [{:xmlel, "action", [{"opcode", "rooms"}], []}]}}
iex(7)> Hedwig.Stanza.to_xml(stanza)                   
"<iq to='server.com' type='get' id='cf8c'><query xmlns='urn:xmpp:muc:light:1'><action opcode='rooms'/></query></iq>"
```
The `Bottles.Muc` provides functions to build stanzas for MUC. You can generate the `docs` to see description of each function. 


* Send the stanza from client:

```elixir
iex(5)> Bottles.send_stanza(client, stanza)
```

* Disconnect the client:

```elixir
iex(6)> Bottles.disconnect_client(client)
:ok
```

### **Handler**
***

In the handler module `lib/receiver.ex` you can see the match of incoming stanzas, for `presence`, `message` and `iq`. Inside these functions you can do whatever you want with the incoming stanzas.

### **Logs**
***

All the logs of incoming and outgoing stanzas will be printed into console.

### **Docs**
***

To generate docs:

```bash
make doc
```
